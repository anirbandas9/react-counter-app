import React, { Component } from "react";
import Counter from "./components/Counter";
import "./App.css";

class App extends Component {
  // My State
  state = {
    counters: [
      {
        id: 1,
        count: 0,
      },
    ],
  };

  // Add Counter
  addCounter = () => {
    const counters = [...this.state.counters];
    const newCounterIndex = counters[counters.length - 1].id + 1;
    counters.push({ id: newCounterIndex, count: 0 });
    this.setState({ counters });
  };

  // Increment Counter
  incrementCount = (id) => {
    // console.log("increment Count", id);
    const counters = [...this.state.counters];
    const index = counters.findIndex((counter) => counter.id === id);
    counters[index].count += 1;
    this.setState({ counters });
  };

  // Decrement Counter
  decrementCount = (id) => {
    // console.log("increment Count", id);
    const counters = [...this.state.counters];
    const index = counters.findIndex((counter) => counter.id === id);
    if (counters[index].count === 0) {
      return;
    } else {
      counters[index].count -= 1;
      this.setState({ counters });
    }
  };

  render() {
    return (
      <div className="App">
        <button onClick={this.addCounter} className="btnStyle">
          Add Counter
        </button>
        {this.state.counters.map((counter) => (
          <Counter
            key={counter.id}
            counter={counter}
            incrementCount={this.incrementCount}
            decrementCount={this.decrementCount}
          />
        ))}
      </div>
    );
  }
}

export default App;
