import React, { Component } from "react";

export class Counter extends Component {
  render() {
    const { id, count } = this.props.counter;
    return (
      <div className="counter">
        <h1>Counter {id}</h1>
        <h2>{count}</h2>
        <button
          onClick={this.props.incrementCount.bind(this, id)}
          className="btnStyle"
        >
          +
        </button>{" "}
        <button
          onClick={this.props.decrementCount.bind(this, id)}
          className="btnStyle"
        >
          -
        </button>
      </div>
    );
  }
}

export default Counter;
